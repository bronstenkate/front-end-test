# Front-end development test

Create a simple survey application which would ask visitor some questions and then **show** results.

## Requirements
* Use ReactJS
* Load questions from provided JSON file (link below)
* BONUS: use `styled-components` package to style with CSS.
* BONUS: make the application compatible for IE11

## Layout
Use CSS to style application to look like in the images below

![Survey view](/img/1.png)

![Result view](/img/2.png)

## Resources
Below are resources you will need to complete the task:

* [JSON file of questions](/questions.json)
* [Normal font](/fonts/blender-pro-normal.otf)
* [Bold font](/fonts/blender-pro-bold.otf)